-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-01-2019 a las 02:26:51
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.0.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cmr_admin`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresas`
--

CREATE TABLE `empresas` (
  `id` int(10) UNSIGNED NOT NULL,
  `razon_social` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usuariosPermitidos` int(11) NOT NULL,
  `cuit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `responsable` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono_responsable` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clave` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clave_p12` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `certificado` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `condicion_fiscal` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `profile_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'dist/empresas/default.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `empresas`
--

INSERT INTO `empresas` (`id`, `razon_social`, `usuariosPermitidos`, `cuit`, `direccion`, `telefono`, `responsable`, `telefono_responsable`, `clave`, `correo`, `clave_p12`, `certificado`, `condicion_fiscal`, `created_at`, `updated_at`, `profile_image`) VALUES
(1, 'Siamest SRL', 3, '20955290454', 'Gallardo John', '2944 400430', 'Isaias Diaz', '2944 793432', 'hil', 'isaarg2312@gmail.com', 'CRT/Siamest SRL/clave.p12', 'CRT/Siamest SRL/clave.p12', 'Monotributista', NULL, '2019-01-18 00:36:01', 'dist/empresas/44916320_2223302467955317_3394656491930648576_n.png'),
(3, 'Alarmas Guerrero', 2, '30711654093', 'Gallardo 699 Bariloche', '2944400381', 'Alarmas Guerrero', '2944600833', '6303', 'guerrero@speedy.com.ar', 'CRT/Alarmas Guerrero/dont.rtf', 'CRT/Alarmas Guerrero/future change.rtf', 'Responsable Inscripto', NULL, NULL, 'dist/empresas/guerrero.jpg'),
(4, 'Pinturerias Del Centro', 10, '30754982021', 'Elflein Bariloche', '2944 4005889', 'Gonzalo Rojas', '29458432', 'C:\\xampp\\tmp\\phpEAA4.tmp', 'gonzalorojas@gmail.com', 'CRT/Pinturerias Del Centro/ISOLOGO COLORES.JPG', 'CRT/Pinturerias Del Centro/ISOLOGO COLORES.JPG', 'Responsable Inscripto', NULL, '2019-01-18 00:24:14', 'dist/empresas/ISOLOGO COLORES.JPG');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(5, '2014_10_12_000000_create_users_table', 1),
(6, '2014_10_12_100000_create_password_resets_table', 1),
(7, '2019_01_10_053133_create_roles_table', 1),
(8, '2019_01_11_203504_create_empresas_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Super Usuario', NULL, NULL),
(2, 'Administrador del Sistema', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `descripcion_puesto` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'dist/users/default.png',
  `ip_client` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `id_categoria` int(11) NOT NULL,
  `empresa_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `descripcion_puesto`, `profile_image`, `ip_client`, `id_categoria`, `empresa_id`) VALUES
(1, 'Isaias Diaz', 'isaarg2312@gmail.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '5ztcCCSCYHfhVcmnXhKLa6TRSCULYTBABXIFsozzr2Cy45SCkOL1x3JJu5s7', '2019-01-17 00:58:00', '2019-01-17 22:42:41', 'Desarrollador Web', 'https://lh6.googleusercontent.com/-FCm1JiSMydI/AAAAAAAAAAI/AAAAAAAAAD8/2kVsl1Qpzo4/photo.jpg?sz=50', '127.0.0.1', 1, 1),
(2, 'Stefania Paiman', 'stefyflow89@gmail.com', '$2y$10$vqNGo2F92pWSL8w6ZgF7LeFAQV5IwHSwaYGk1/SOZS4Xpa.Bx6NLW', 'KVeLHVuIl9wA0UrFv6X5Fn7314VkCeLdJjPuycRYIonrrYYIKTkviHSVgpNO', NULL, '2019-01-17 22:43:03', 'Maquetadora Web', 'https://lh3.googleusercontent.com/-mGTPpIs_rFg/AAAAAAAAAAI/AAAAAAAAOgg/9CM4XAWzFCo/photo.jpg?sz=50', '127.0.0.1', 2, 1),
(3, 'Maria Velazquez', 'maria@gmail.com', '$2y$10$.aVRqxbGXmTFI0YDCcZqxOWEQ9CYMCLGIW2gxHPekcx4peMaRhFnu', NULL, NULL, NULL, 'Suegra', 'dist/users/default.png', '0', 2, 1),
(4, 'Matias', 'matias@gmail.com', '$2y$10$yDT7TbJ24deLZh6OXPQBj.uTcrQuIXfO6uU.PmJfw7yVNMjGwQFVS', NULL, NULL, NULL, 'Partner', 'dist/users/default.png', '0', 2, 1),
(5, 'Marco', 'marco@gmail.cliente', '$2y$10$eATNLSn5wqvwWAZN85it9O6wbEwu0iFJaRN/m67QhyKUc5rGB6Fhy', NULL, NULL, NULL, 'prueba', 'dist/users/default.png', '0', 2, 1),
(6, 'Marisol Velazquez', 'marisol@gmail.com', '$2y$10$6ARyT3ZyW8EMd9ugEBmjv.hpQrWbLhc0ZCS7gv3KRyCNAI/C8.TKK', NULL, NULL, NULL, 'Abuela de Stefy', 'dist/users/default.png', '0', 2, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `empresas`
--
ALTER TABLE `empresas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `empresas_cuit_unique` (`cuit`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `empresas`
--
ALTER TABLE `empresas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
