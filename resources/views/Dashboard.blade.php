
@extends( auth()->user()->id_categoria == 1 ? 'Layout/_LayoutSU' : 'Layout/_Layout')

@section('ProfiImage')
{{ auth()->user()->profile_image }}
@endsection

@section('empresaUth')
{{auth()->user()->empresa->razon_social}}
@endsection

@section('descripcion_puesto')
   {{auth()->user()->descripcion_puesto}}
@endsection

@section('namesidebar')
   {{ auth()->user()->name }}
@endsection

@section('wrapper')
<meta name="csrf-token" content="{{ csrf_token() }}">
<script>
    $(document).ready(function(){
        $('.treeview').removeClass('active');
        $('#das').addClass('active');
        $('#das1').addClass('active');

            $('#fact_test').click(function(){
              $.ajaxSetup({
                 headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                   }
               });
               var vari=1;
               var data="data="+vari;
              $.ajax({
                  method: "post",
                  data: data,
                  url: "<?php echo route('facturar_producto')?>",
                  success: function(response){ // What to do if we succeed
                    swal("Buen Trabajo!", response, "success");

                        }

                    });

            });
    });

</script>
<section class="content-header">
  <h1>
    {{auth()->user()->empresa->razon_social}}
    <small>Panel de Control</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fab fa-dashboard"></i> Inicio</a></li>
    <li class="active">Dashboard</li>
  </ol>
</section>

<section class="content">
  <div class="row">

    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>150</h3>

          <p>Ordenes de Compra</p>
        </div>
        <div class="icon">
          <i class="ion ion-bag"></i>
        </div>
        <a href="#" class="small-box-footer">Mas información <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>


    <div class="col-lg-3 col-xs-6">

      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>44</h3>

          <p>Usuarios Registrados</p>
        </div>
        <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>
        <a href="#" class="small-box-footer">Mas Information <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>

  </div>
</section>
<button class="btn btn-primary" id="fact_test">Facturar</button>
@endsection
