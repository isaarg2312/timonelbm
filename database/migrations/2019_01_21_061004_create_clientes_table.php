<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('direccion1');
            $table->string('direccion2');
            $table->string('cel1');
            $table->string('cel2');
            $table->string('fijo');
            $table->string('documento');
            $table->string('direccion_fiscal');
            $table->string('condicion_fiscal');
            $table->integer('empresa_id');
            $table->string('profile_image')->default('dist/users/default.png');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
