<?php

namespace App\Http\Controllers;
use App\Role;
use App\User;
use App\Empresa;
use App\Cliente;
use App\Servicio;
use App\Producto;
use App\Message;
use App\Task;
use App\TaskUser;
use App\Notification;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\App;

class HomeController extends Controller
{
  var $pusher;
 var $user;
 var $chatChannel;

 const DEFAULT_CHAT_CHANNEL = 'chat';

 public function __construct()
 {

     $this->chatChannel = self::DEFAULT_CHAT_CHANNEL;
 }

public function notificaciones(){
  if(Auth::check()){
    $id=auth()->user()->empresa_id;
  // $user=User::where('id',$id);
$notificaciones=Notification::where('empresa_id',$id)->get();

       return view('Notificaciones',compact('notificaciones'));
  }else{
    $user=User::where('ip_client',\Request::ip())->first();
    if($user!=null){
      $profile_image=$user->profile_image;
      $email=$user->email;
      $name=$user->name;
      return view('lockscreen',['name'=>$name,'profile'=>$profile_image,'email'=>$email]);
    }else{
      return view('/');
    }
  }
}
public function listacalendario(){
  if(Auth::check()){
    $id=auth()->user()->id;
  // $user=User::where('id',$id);
       $tasks=TaskUser::where('user_id',$id)->get();
       $usuarios=User::where('empresa_id',auth()->user()->empresa_id)->get();
      // $usuariosJ=collect($usuarios);
      // $usuariosJ=$usuariosJ->toArray();
       return view('ListaCalendario',compact('tasks','usuarios'));
  }else{
    $user=User::where('ip_client',\Request::ip())->first();
    if($user!=null){
      $profile_image=$user->profile_image;
      $email=$user->email;
      $name=$user->name;
      return view('lockscreen',['name'=>$name,'profile'=>$profile_image,'email'=>$email]);
    }else{
      return view('/');
    }
  }
}
 public function calendario(){
   if(Auth::check()){
     $id=auth()->user()->id;
     $tasks=TaskUser::where('user_id',$id)->get();

            return view('Calendario',compact('tasks'));

   }else{
     $user=User::where('ip_client',\Request::ip())->first();
     if($user!=null){
       $profile_image=$user->profile_image;
       $email=$user->email;
       $name=$user->name;
       return view('lockscreen',['name'=>$name,'profile'=>$profile_image,'email'=>$email]);
     }else{
       return view('/');
     }
   }
 }
  public function chat(){
    if(Auth::check()){

         $usuarios=User::All();
         $mensajes=Message::All();
         return view('Chat',['chatChannel' => $this->chatChannel],compact('usuarios','mensajes'));
    }else{
      $user=User::where('ip_client',\Request::ip())->first();
      if($user!=null){
        $profile_image=$user->profile_image;
        $email=$user->email;
        $name=$user->name;
        return view('lockscreen',['name'=>$name,'profile'=>$profile_image,'email'=>$email]);
      }else{
        return view('/');
      }
    }
  }

  public function ClientesAgenda(){
     if(Auth::check()){

        $clientes=Cliente::where('empresa_id',auth()->user()->empresa_id)->get();
        $servicios=Servicio::where('empresa_id',auth()->user()->empresa_id)->get();
       return view('ABMS/clientes',compact('clientes','servicios'));
     }else{
         $user=User::where('ip_client',\Request::ip())->first();
         if($user!=null){
           $profile_image=$user->profile_image;
           $email=$user->email;
           $name=$user->name;
           return view('lockscreen',['name'=>$name,'profile'=>$profile_image,'email'=>$email]);
         }else{
           return view('/');
         }

     }

  }
  public function Serviciosini(){

    if(Auth::check()){

       $productos=Producto::where('empresa_id', auth()->user()->empresa_id)->get();
       $servicios=Servicio::where('empresa_id', auth()->user()->empresa_id)->get();
      return view('ABMS/servicios',compact('productos','servicios'));
    }else{
        $user=User::where('ip_client',\Request::ip())->first();
        if($user!=null){
          $profile_image=$user->profile_image;
          $email=$user->email;
          $name=$user->name;
          return view('lockscreen',['name'=>$name,'profile'=>$profile_image,'email'=>$email]);
        }else{
          return view('/');
        }

      }

  }
    public function usuariosEmpresa(){
        if (Auth::check()) {
            $users=User::all();
            $roles=Role::all();
            $empresas=Empresa::all();

            return view('ABMS/usuariosE',compact('users','roles','empresas'));

        }else{
            $User = User::where('ip_client', \Request::ip())->first();
            if($User!=null){
                $profile_image=$User->profile_image;
                $email=$User->email;
                $name=$User->name;
                return view('lockscreen',['name' => $name,'profile'=>$profile_image,'email'=>$email]);
            }else{
                return view('/');
            }
        }
    }

 public function inicio(){
    if (Auth::check()) {

        return view('Dashboard');
    }else{
        return view('Home');
    }

 }

 public function empresa(){
    if (Auth::check()) {
        $empresas=Empresa::all();

        return view('ABMS/Empresa',compact('empresas'));
    }else{
        return view('Home');
    }
 }

 public function initsesion(){
    if (Auth::check()) {
        return view('Dashboard');
    }else{
        $User = User::where('ip_client', \Request::ip())->first();
        if($User!=null){
            $profile_image=$User->profile_image;
            $email=$User->email;
            $name=$User->name;
            return view('lockscreen',['name' => $name,'profile'=>$profile_image,'email'=>$email]);
        }else{
            return view('Home');
        }
    }
 }

 public function Usuarios(){
    if (Auth::check()) {

        $roles=Role::all();


        $users = User::where('empresa_id', auth()->user()->empresa_id)->get();
        return view('ABMS/Usuarios',compact('users','roles'));

    }else{
        $User = User::where('ip_client', \Request::ip())->first();
        if($User!=null){
            $profile_image=$User->profile_image;
            $email=$User->email;
            $name=$User->name;
            return view('lockscreen',['name' => $name,'profile'=>$profile_image,'email'=>$email]);
        }else{
            return view('/');
        }
    }
 }
 public function lockscreen(){
         return view('lockscreen');
 }

}
