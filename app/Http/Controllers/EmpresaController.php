<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Empresa;
use File;
class EmpresaController extends Controller
{

    public function delete(Request $request){
              $id=$request->id;
              $empresa=Empresa::where("id",$id)->first();
              $name=$empresa->razon_social;
              $clave=$empresa->clave_p12;
              $certificado=$empresa->certificado;

              Storage::delete([$clave, $certificado]);
              $empresa->delete();
              return  "Empresa Eliminada";


    }

    public function edit(Request $request){
        $id=$request->id;                 
        $empresa=Empresa::where("id",$id)->first();
        return response()->json($empresa);
    }
    public function add(Request $request){
        $control=$request->control;
        //si esta en cero es por que no se ha seteado ningún id en la funcion load data que es donde se abre el modal con los datos
        //de la empresa seleccionada.
        
        if($control==0){

            $test=$request->razon_social;

            if($request->hasFile('clave_en')&&$request->hasFile('cert')){
                    
                $clave= $request->file('clave_en');
                $cert= $request->file('cert');
                $prof= $request->file('profile_image');

                $clavename=$clave->getClientOriginalName();
                $certname=$cert->getClientOriginalName();
                $profname=$prof->getClientOriginalName();
                /*
                $clave->move(storage_path().);
                $cert->move(storage_path().);*/
        
                $destino=public_path('/dist/empresas');
                $prof->move($destino,$profname);

                $path = $request->file('clave_en')->storeAS(
                    'CRT/'.$test,$clavename
                );
        
                $path1 = $request->file('cert')->storeAS(
                    'CRT/'.$test,$certname
                );       
                     
                
        
                Empresa::insert([
                        "razon_social"=>$request->razon_social,
                        "usuariosPermitidos"=>$request->usuariosPermitidos,
                        "cuit"=>$request->cuit,
                        "direccion"=>$request->direccion,
                        "telefono"=>$request->telefono,
                        "responsable"=>$request->responsable,
                        "telefono_responsable"=>$request->telefono_responsable,
                        "correo"=>$request->correo,
                        "clave_p12"=>$path,
                         "certificado"=>$path1,
                         "profile_image"=>"dist/empresas/".$profname,
                        "clave"=>$request->clave,
                        "condicion_fiscal"=>$request->condicion_fiscal,
                ]);
        /*
                $path1 = $request->file('clave_en')->store($clavename,'CRT');
                $path = $request->file('cert')->store($certname,'CRT');*/
                $empresas=Empresa::all();
        
                 
                return redirect()->to('empresa');
            }else{
                $empresas=Empresa::all();
        
                return redirect()->to('empresa');
            }
        
            
        }else{
            $razon_social=$request->razon_social;
            $usuariosPermitidos=$request->usuariosPermitidos;
            $cuit=$request->cuit;
            $direccion=$request->direccion;
            $telefono=$request->telefono;
            $responsable=$request->responsable;
            $telefono_responsable=$request->telefono_responsable;
            $correo=$request->correo;
            $clave=$request->clave;
            $condicion_fiscal=$request->condicion_fiscal;
            
            if($request->hasFile('clave_en')&&$request->hasFile('cert')&&$request->hasFile('profile_image')){
                $id=$request->control;                
                $empresa=Empresa::where("id",$id)->first();
                $test=$request->razon_social;
                $name=$empresa->razon_social;
                 $clave=$empresa->clave_p12;
                  $certificado=$empresa->certificado;

                  Storage::delete([$clave, $certificado]);
                  File::delete(public_path().$empresa->profile_image);

                 $clave= $request->file('clave_en');
                $cert= $request->file('cert');
                $prof= $request->file('profile_image');

                $profname=$prof->getClientOriginalName();
                $clavename=$clave->getClientOriginalName();
                $certname=$cert->getClientOriginalName();
                /*
                $clave->move(storage_path().);
                $cert->move(storage_path().);*/
        
                $destino=public_path('/dist/empresas');
                $prof->move($destino,$profname);
                

                $path = $request->file('clave_en')->storeAS(
                    'CRT/'.$test,$clavename
                );
        
                $path1 = $request->file('cert')->storeAS(
                    'CRT/'.$test,$certname
                ); 

                   $empresa->razon_social=$razon_social;
                   $empresa->usuariosPermitidos=$usuariosPermitidos;
                   $empresa->cuit=$cuit;
                   $empresa->direccion=$direccion;
                   $empresa->telefono=$telefono;
                   $empresa->responsable=$responsable;
                   $empresa->telefono_responsable=$telefono_responsable;
                   $empresa->correo=$correo;
                   $empresa->clave_p12=$path;
                   $empresa->certificado=$path1;
                   $empresa->clave=$clave;
                   $empresa->condicion_fiscal=$condicion_fiscal;
                   $empresa->profile_image="dist/empresas/".$profname;
                   $empresa->save();
                   return redirect()->to('empresa');
            }else{
                               
                $empresa=Empresa::where("id",$control)->first();
                $empresa->razon_social=$razon_social;
                $empresa->usuariosPermitidos=$usuariosPermitidos;
                $empresa->cuit=$cuit;
                $empresa->direccion=$direccion;
                $empresa->telefono=$telefono;
                $empresa->responsable=$responsable;
                $empresa->telefono_responsable=$telefono_responsable;
                $empresa->correo=$correo;
                
                $empresa->clave=$clave;
                $empresa->condicion_fiscal=$condicion_fiscal;
                $empresa->save();
                return redirect()->to('empresa');
            }
        }         
    }
}