<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Correo;
use Telefono;
use Factura;

class Cliente extends Model
{
    public function facturas()
    {
     return $this->hasMany('App\Factura');
    }
    public function servicios()
    {
         return $this->belongsToMany('App\servicio');
    }
    public function correos()
    {
      return $this->hasMany('App\Correo');
    }
    public function telefonos()
    {
        return $this->hasMany('App\Telefono');
    }
    public function empresa()
    {
         return $this->belongsTo('App\Empresa');
    }
    //Coments to branch
}
