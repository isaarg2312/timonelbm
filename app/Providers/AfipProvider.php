<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AfipProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
      $this->app->singleton('App\Afip', function ($app) {
        return new Afip(new ElectronicBilling(config('secret_key')), new ClaseC(new ClaseX(), new ClaseY()), new ClaseD('pym', 7));
       });
    }
}
