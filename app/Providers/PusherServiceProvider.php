<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class PusherServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        \App::bind('Pusher');
        \App::bind('PusherCrypto');
        \App::bind('PusherException');
        \App::bind('PusherInstance');
        \App::bind('Webhook');

    }
}
