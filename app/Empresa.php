<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{

    public function tasks(){
      return $this->hasMany('App\Task');
    }
    public function users()
    {
        return $this->hasMany('App\User');
    }
    public function clientes()
    {
      return $this->hasMany('App\Cliente');
    }
    public function oportunidades()
    {
      return $this->hasMany('App\Oportunidade');
    }
    public function proveedores()
    {
      return $this->hasMany('App\Oportunidade');
    }
    public function notifications()
    {
      return $this->hasMany('App\Notification');
    }
    public function productos()
    {
      return $this->hasMany('App\Producto');
    }
    public function servicios()
    {
      return $this->hasMany('App\Servicio');
    }
    public function comentarios(){
      return $this->hasMany('App\Comentario');
    }
}
