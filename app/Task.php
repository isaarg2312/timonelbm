<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Comentario;

class Task extends Model
{
    public function users(){
      return $this->hasMany(User::class);
    }
    public function empresa(){
      return $this->belongsTo(Empresa::class);
    }
    public function comentarios(){
      return $this->hasMany(Comentario::class);
    }
}
